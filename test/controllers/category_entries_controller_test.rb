require 'test_helper'

class CategoryEntriesControllerTest < ActionController::TestCase
  setup do
    @category_entry = category_entries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:category_entries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create category_entry" do
    assert_difference('CategoryEntry.count') do
      post :create, category_entry: { category_entry_id: @category_entry.category_entry_id, kind_entry_id: @category_entry.kind_entry_id, name: @category_entry.name }
    end

    assert_redirected_to category_entry_path(assigns(:category_entry))
  end

  test "should show category_entry" do
    get :show, id: @category_entry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @category_entry
    assert_response :success
  end

  test "should update category_entry" do
    patch :update, id: @category_entry, category_entry: { category_entry_id: @category_entry.category_entry_id, kind_entry_id: @category_entry.kind_entry_id, name: @category_entry.name }
    assert_redirected_to category_entry_path(assigns(:category_entry))
  end

  test "should destroy category_entry" do
    assert_difference('CategoryEntry.count', -1) do
      delete :destroy, id: @category_entry
    end

    assert_redirected_to category_entries_path
  end
end
