require 'test_helper'

class KindEntriesControllerTest < ActionController::TestCase
  setup do
    @kind_entry = kind_entries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kind_entries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kind_entry" do
    assert_difference('KindEntry.count') do
      post :create, kind_entry: { description: @kind_entry.description }
    end

    assert_redirected_to kind_entry_path(assigns(:kind_entry))
  end

  test "should show kind_entry" do
    get :show, id: @kind_entry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kind_entry
    assert_response :success
  end

  test "should update kind_entry" do
    patch :update, id: @kind_entry, kind_entry: { description: @kind_entry.description }
    assert_redirected_to kind_entry_path(assigns(:kind_entry))
  end

  test "should destroy kind_entry" do
    assert_difference('KindEntry.count', -1) do
      delete :destroy, id: @kind_entry
    end

    assert_redirected_to kind_entries_path
  end
end
