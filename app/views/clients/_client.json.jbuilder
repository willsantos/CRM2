json.extract! client, :id, :codDesc, :name, :brand, :CNAE, :email, :site, :capital, :kind_id, :created_at, :updated_at
json.url client_url(client, format: :json)
