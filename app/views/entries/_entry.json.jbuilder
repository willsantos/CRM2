json.extract! entry, :id, :description, :date_entry, :amount, :category_entry_id, :client_id, :created_at, :updated_at
json.url entry_url(entry, format: :json)
