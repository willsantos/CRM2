json.extract! category_entry, :id, :name, :kind_entry_id, :category_entry_id, :created_at, :updated_at
json.url category_entry_url(category_entry, format: :json)
