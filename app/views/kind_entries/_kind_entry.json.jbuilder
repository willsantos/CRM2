json.extract! kind_entry, :id, :description, :created_at, :updated_at
json.url kind_entry_url(kind_entry, format: :json)
