json.extract! kind, :id, :abbr, :description, :created_at, :updated_at
json.url kind_url(kind, format: :json)
