json.extract! phone, :id, :prefix, :number, :kind, :client_id, :created_at, :updated_at
json.url phone_url(phone, format: :json)
