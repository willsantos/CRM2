class Client < ActiveRecord::Base
  belongs_to :kind
  has_many   :phones
  has_many   :partners
  has_one    :address
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :phones, reject_if: :all_blank,allow_destroy: true 

end
