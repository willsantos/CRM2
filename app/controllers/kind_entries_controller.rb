class KindEntriesController < ApplicationController
  before_action :set_kind_entry, only: [:show, :edit, :update, :destroy]

  # GET /kind_entries
  # GET /kind_entries.json
  def index
    @kind_entries = KindEntry.all
  end

  # GET /kind_entries/1
  # GET /kind_entries/1.json
  def show
  end

  # GET /kind_entries/new
  def new
    @kind_entry = KindEntry.new
  end

  # GET /kind_entries/1/edit
  def edit
  end

  # POST /kind_entries
  # POST /kind_entries.json
  def create
    @kind_entry = KindEntry.new(kind_entry_params)

    respond_to do |format|
      if @kind_entry.save
        format.html { redirect_to @kind_entry, notice: 'Kind entry was successfully created.' }
        format.json { render :show, status: :created, location: @kind_entry }
      else
        format.html { render :new }
        format.json { render json: @kind_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kind_entries/1
  # PATCH/PUT /kind_entries/1.json
  def update
    respond_to do |format|
      if @kind_entry.update(kind_entry_params)
        format.html { redirect_to @kind_entry, notice: 'Kind entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @kind_entry }
      else
        format.html { render :edit }
        format.json { render json: @kind_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kind_entries/1
  # DELETE /kind_entries/1.json
  def destroy
    @kind_entry.destroy
    respond_to do |format|
      format.html { redirect_to kind_entries_url, notice: 'Kind entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kind_entry
      @kind_entry = KindEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kind_entry_params
      params.require(:kind_entry).permit(:description)
    end
end
