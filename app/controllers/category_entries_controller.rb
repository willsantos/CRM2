class CategoryEntriesController < ApplicationController
  before_action :set_category_entry, only: [:show, :edit, :update, :destroy]

  # GET /category_entries
  # GET /category_entries.json
  def index
    @category_entries = CategoryEntry.all
  end

  # GET /category_entries/1
  # GET /category_entries/1.json
  def show
  end

  # GET /category_entries/new
  def new
    @category_entry = CategoryEntry.new
  end

  # GET /category_entries/1/edit
  def edit
  end

  # POST /category_entries
  # POST /category_entries.json
  def create
    @category_entry = CategoryEntry.new(category_entry_params)

    respond_to do |format|
      if @category_entry.save
        format.html { redirect_to @category_entry, notice: 'Category entry was successfully created.' }
        format.json { render :show, status: :created, location: @category_entry }
      else
        format.html { render :new }
        format.json { render json: @category_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_entries/1
  # PATCH/PUT /category_entries/1.json
  def update
    respond_to do |format|
      if @category_entry.update(category_entry_params)
        format.html { redirect_to @category_entry, notice: 'Category entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_entry }
      else
        format.html { render :edit }
        format.json { render json: @category_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_entries/1
  # DELETE /category_entries/1.json
  def destroy
    @category_entry.destroy
    respond_to do |format|
      format.html { redirect_to category_entries_url, notice: 'Category entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_entry
      @category_entry = CategoryEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_entry_params
      params.require(:category_entry).permit(:name, :kind_entry_id, :category_entry_id)
    end
end
