class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.integer :prefix
      t.integer :number
      t.integer :kind
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
