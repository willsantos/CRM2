class CreateKindEntries < ActiveRecord::Migration
  def change
    create_table :kind_entries do |t|
      t.string :description

      t.timestamps null: false
    end
  end
end
