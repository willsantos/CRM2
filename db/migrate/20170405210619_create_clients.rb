class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.integer :codDesc
      t.string :name
      t.string :brand
      t.integer :CNAE
      t.string :email
      t.string :site
      t.decimal :capital
      t.references :kind, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
