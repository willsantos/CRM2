class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.string :phone
      t.integer :capital
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
