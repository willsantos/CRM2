class CreateCategoryEntries < ActiveRecord::Migration
  def change
    create_table :category_entries do |t|
      t.string :name
      t.references :kind_entry, index: true, foreign_key: true
      t.references :category_entry, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
