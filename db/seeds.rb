# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


puts "Gerando Tipos de empresas..."

Kind.create!(abbr:'ME',description: 'Micro Empresa')
Kind.create!(abbr:'MEI',description: 'Micro Empreendedor Individual')
Kind.create!(abbr:'EPP',description: 'Empresa de Pequeno Porte')
Kind.create!(abbr:'EIRELI',description: 'Empresa Individual de Responsabilidade Limitada')
Kind.create!(abbr:'SA',description: 'Empresa de Sociedade Anonima')

puts "Gerando Tipos de empresas...[ok]"


puts "Gerando Empresas Default..."

Client.create!(codDesc:'90',	name:'PHX WEB LTDA',	brand:'PHX WEB',	CNAE:'1245',	email:'contato@phxweb.com.br',	site:'www.phxweb.com.br',	capital:3000,	kind_id:1)
Client.create!(codDesc:'87',	name:'AJ dos Santos LTDA',	brand:'Contabil Santos',	CNAE:'1245',	email:'contato@contabilsantos.com',	site:'www.contabilsantos.com',	capital:0,	kind_id:1	)

puts "Gerando Empresas Default...[ok]"

puts "Gerando Empresas Fake (Clients)..."
      100.times do |i|

        Client.create!(codDesc:Faker::Number.number(3),	name:Faker::Company.name,	brand:Faker::Company.buzzword,	CNAE:Faker::Company.ein,	email:Faker::Internet.email,	site:Faker::Internet.url,	capital:Faker::Number.decimal(4,2),	kind_id:Faker::Number.between(1, 5))

      end
puts "Gerando Empresas Fake (Clients)...[Ok]"


puts "Gerando os endereços (Addresses)..."
      Client.all.each do |client|
        Address.create!(
          street: Faker::Address.street_address,
          city: Faker::Address.city,
          state: Faker::Address.state_abbr,
          client: client
        )
      end
puts "Gerando os endereços (Addresses)... [OK]"

puts "Gerando os telefones (Phones)..."
      Client.all.each do |client|
        Random.rand(1..5).times do |i|
          Phone.create!(
            prefix: Faker::Number.number(3),
            number: Faker::Number.number(8),
            kind: Faker::Number.between(0,1),
            client: client
          )
        end
      end
    puts "Gerando os telefones (Phones)... [OK]"
