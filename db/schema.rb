# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170412184433) do

  create_table "addresses", force: :cascade do |t|
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "district"
    t.string   "country"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "addresses", ["client_id"], name: "index_addresses_on_client_id"

  create_table "category_entries", force: :cascade do |t|
    t.string   "name"
    t.integer  "kind_entry_id"
    t.integer  "category_entry_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "category_entries", ["category_entry_id"], name: "index_category_entries_on_category_entry_id"
  add_index "category_entries", ["kind_entry_id"], name: "index_category_entries_on_kind_entry_id"

  create_table "clients", force: :cascade do |t|
    t.integer  "codDesc"
    t.string   "name"
    t.string   "brand"
    t.integer  "CNAE"
    t.string   "email"
    t.string   "site"
    t.decimal  "capital"
    t.integer  "kind_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "clients", ["kind_id"], name: "index_clients_on_kind_id"

  create_table "entries", force: :cascade do |t|
    t.string   "description"
    t.date     "date_entry"
    t.decimal  "amount"
    t.integer  "category_entry_id"
    t.integer  "client_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "entries", ["category_entry_id"], name: "index_entries_on_category_entry_id"
  add_index "entries", ["client_id"], name: "index_entries_on_client_id"

  create_table "kind_entries", force: :cascade do |t|
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "kinds", force: :cascade do |t|
    t.string   "abbr"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "partners", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.integer  "capital"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "partners", ["client_id"], name: "index_partners_on_client_id"

  create_table "phones", force: :cascade do |t|
    t.integer  "prefix"
    t.integer  "number"
    t.integer  "kind"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "phones", ["client_id"], name: "index_phones_on_client_id"

end
